import $http from '../http.js'
export const get = (data) => {	
	return $http({
		url:'get',
		data
	})
}


export const get_user = (data) => {
	return $http({
		url:'get_user',
		data
	})
}

export const update_feedback=(data)=>{
	return $http({
		url:'update_feedback',
		data
	})
}