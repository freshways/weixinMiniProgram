export default function $http(options) {
	const {
		url,
		data
	} = options
	console.log(data);
	return new Promise((reslove, reject) => {
		uniCloud.callFunction({
			name: url,
			data
		}).then((res) => {
			console.log("====promise====");
			console.log(res);
			if (res.result.code === 200) {
				//then
				reslove(res.result)
			} else {
				//catch
				reject(res.result)
			}
		}).catch(
			(err) => reject(err)
		)
	})
}
