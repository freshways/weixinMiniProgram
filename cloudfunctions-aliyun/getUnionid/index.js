'use strict';

exports.main = async (event, context) => {
	console.log("开始异步请求getunionid");
	const apiUrl = "https://api.weixin.qq.com/sns/jscode2session";
	const res = await uniCloud.httpclient.request(apiUrl,
				{			
					method: "GET",
					dataType:"json",
					data: {
						"grant_type" : "authorization_code",
						"appid"	  : "wxd4b4ca5658bd565c",						             
						"secret"  : "6f6ca5e415ba4594f2a37eb038816f2e",						             
						"js_code" : event.js_code
					}
				});
	console.log("getunionid:")
	console.log(res)
	//event为客户端上传的参数
	console.log(event)
	//返回数据给客户端
	// return event;
	return res
};
