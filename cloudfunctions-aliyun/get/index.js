'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	// console.log(event.openid);
	const collection = db.collection('list');

	const res = await collection.where({
		openid: event.openid
	}).get();

	// console.log(res);
	//const res = await collection.limit(10).where().get()
	// const res = await collection.where({
	// 	openid:event.openid
	// }).orderBy("_id", "desc").get()
	return {
		code: 200,
		msg: '请求成功',
		data: res.data
	}
};
